package chat.support;

import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Encryption {
	// Keys and ciphers
	private static SecretKey m_key;
	private static Cipher m_ecipher;
	private static Cipher m_dcipher;
	
	static {
		try {
			// Create key
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			secureRandom.setSeed(0xABCDDCBA);
			KeyGenerator keyGenerator;
			keyGenerator = KeyGenerator.getInstance("DES");
			keyGenerator.init(secureRandom);
			m_key = keyGenerator.generateKey();
			
			// Create and initialize ciphers
			m_ecipher = Cipher.getInstance("DES");
            m_dcipher = Cipher.getInstance("DES");
            m_ecipher.init(Cipher.ENCRYPT_MODE, m_key);
            m_dcipher.init(Cipher.DECRYPT_MODE, m_key);
		} catch (Exception e) {
			System.out.println("Encryption initialization failed!");
		}		
	}
	
	public static synchronized String encrypt(String input){
        try {
            // Encrypt
            byte[] utf8 = input.getBytes("UTF8");
            byte[] enc = m_ecipher.doFinal(utf8);
            return (Base64.getEncoder().encodeToString(enc));
        } catch (Exception e) {
			System.out.println("Message encryption failed!");
			e.printStackTrace();
        	return "***";
		}
	}
	public static synchronized String decrypt(String input){
		try {
            // Decrypt
			byte[] dec = Base64.getDecoder().decode(input);
			byte[] utf8 = m_dcipher.doFinal(dec);
            return new String(utf8, "UTF8");            
		} catch (Exception e) {
			System.out.println("Message decryption failed!");
			e.printStackTrace();
        	return "***";
		}
	}

}
