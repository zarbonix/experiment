package chat.support;

public class Scenario3Client extends Client {
	
	private static final long serialVersionUID = 1802830256455389850L;
	
	protected String password = "";
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
