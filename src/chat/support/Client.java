package chat.support;

import java.io.Serializable;

public class Client implements Serializable{
	
	private static final long serialVersionUID = 1L;
	protected int iIdentifier;     // unique identifier
	protected String sName;        // chat name
	protected String sHost;        // client name
	protected String sAddress;     // client address
	protected Callback oCallback;  // client callback
	
	public int getIIdentifier() {
		return iIdentifier;
	}
	public void setIIdentifier(int identifier) {
		iIdentifier = identifier;
	}
	public String getSName() {
		return sName;
	}
	public void setSName(String name) {
		sName = name;
	}
	public String getSHost() {
		return sHost;
	}
	public void setSHost(String host) {
		sHost = host;
	}
	public String getSAddress() {
		return sAddress;
	}
	public void setSAddress(String address) {
		sAddress = address;
	}
	public Callback getOCallback() {
		return oCallback;
	}
	public void setOCallback(Callback callback) {
		oCallback = callback;
	}

}
	