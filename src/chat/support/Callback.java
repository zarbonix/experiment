/**
 * 
 */
package chat.support;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface Callback extends Remote {
	// Transmit message to clients
    public void transmit(Message message) throws RemoteException;

    // Configure chat clients
    public void configure(Group group) throws RemoteException;
}