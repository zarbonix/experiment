package chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

import chat.support.Client;
import chat.support.Message;
import chat.support.Status;

//
// Remote interface between chat server and chat client
//
public interface IServerIM extends Remote {
	// Called by clients to attach
    public int attach(Client client) throws RemoteException;

    // Called by clients to detach
    public void detach(Client client) throws RemoteException;
    
    // Called by clients to send message
    public void send(Message message) throws RemoteException;
    
    public void shutdown() throws RemoteException;

    // Called by clients retrieve status
    public Status dump() throws RemoteException;
    
    public Status _dump() throws RemoteException;
    
    public boolean initialize(int init) throws RemoteException;
}
