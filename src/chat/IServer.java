package chat;

//
// Local interface between server user interface and server
//
public interface IServer {
	// Send message from server to user interface
	public void message(String contents);
	
	// Update user interface statistics from chat server
	public void update(int clients, int groups, int messages, int size);
}
