package chat;

//
// Local interface between client user interface and client
//
public interface IClient {
	// Receive error from client
	public void error(String error);

	// Receive warning from client
	public void warning(String warning);

	// Receive messages from chat server
	public void receive(String name, String contents);
}
