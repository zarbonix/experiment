package chat.impl;

import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import chat.IClient;
import chat.IClientIM;
import chat.IServerIM;
import chat.support.Callback;
import chat.support.Client;
import chat.support.Group;
import chat.support.Message;
import chat.support.Status;

public class StandardClientImpl extends Thread implements Callback, IClientIM {
	//
	// Member variables
	//
	
	// Client data structure
	protected Client m_client;
	
    // Client chat group
	protected Group m_group;
	
	// Remote server interface
	protected IServerIM m_remote;
	
	// User interface callbacks
	protected IClient m_ui;
	
	// Latest message received
	protected int m_id = -1;
	
	// Number messages received
	protected int m_messages = 0;
	
	// Abort flag
	protected boolean m_abort = false;
	
	//
	// Thread safe, server is producer, client is consumer
	//
	// Client command
	protected class Command {
		public Command() {
			message  = null;
			group  = null;
		}
		Message message;
		Group group;
	}
	// Command queue
	protected ConcurrentLinkedQueue<Command> m_queueCommands;

	// Client constructor
	public StandardClientImpl(IClient clientInterface) {
		// Store user interface callback
		m_ui = clientInterface;
		m_client = new Client();
		// Create containers
		m_queueCommands = new ConcurrentLinkedQueue<Command>();
    }
	
	//
	// Remote methods (callbacks)
	//

	// Transmit chat message
	// @SpecData(allow={"ArgumentPassing"})
	/* (non-Javadoc)
	 * @see chat.IClientIM#transmit(chat.IRemote.Message)
	 */
	public synchronized void transmit(Message message) throws RemoteException {
		// Abort, if requested
		if (m_abort){
			throw new RemoteException();
		}

		// Queue command and notify
		Command command = new Command();
		command.message = message;
		m_queueCommands.add(command);
		notify();
    }
	
	// Chat client configuration
    /* (non-Javadoc)
	 * @see chat.IClientIM#configure(chat.IRemote.Group)
	 */
    public synchronized void configure(Group group) throws RemoteException {
		// Abort, if requested
		if (m_abort) {
			throw new RemoteException();
		}

		// Queue command and notify
		Command command = new Command();
		command.group = group;
		m_queueCommands.add(command);
		notify();
    }

    // Main client thread
	/* (non-Javadoc)
	 * @see chat.IClientIM#run()
	 */
	public synchronized void run() {
		// Send user interface message
		m_ui.warning("Chat client is ready to send and receive messages");

		// Check queues
		while (true){
			// Wait for commands
			try {
				wait();
			} catch (Exception e) {
			}

			// Process messages
			while (!m_queueCommands.isEmpty()){
				Command command = m_queueCommands.remove();
				if (command.message != null){
					// Ignore duplicate messages
					if (command.message.getIIdentifier() > m_id){
						// Increment number of messages
						m_messages++;
						
						// Send to user interface
						m_ui.receive(command.message.getSName(), command.message.getSContents());

						// Transmit message to clients
						retransmit(command.message);

						// Update latest message
						m_id = command.message.getIIdentifier();
					}
				}
				else if (command.group != null){
					// Store group
			    	m_group = command.group;
				}
			}
		}
	}

    //
    // Public methods
    //

	// Initialize client
	/* (non-Javadoc)
	 * @see chat.IClientIM#initialize(java.lang.String, java.lang.String)
	 */
	public boolean initialize(String sServer, String sName){
        // Create and install a security manager 
		if (System.getSecurityManager() == null) System.setSecurityManager(new SecurityManager()); 
		
        try {
        	// Define service name
        	String name = "ChatServer";

        	// Locate rmi registry
        	Registry registry = LocateRegistry.getRegistry(sServer);

        	// Attach to remote object
        	m_remote = (IServerIM) registry.lookup(name);

        	// Export remote methods
			UnicastRemoteObject.exportObject(this, 0);

			// Setup client
	
			m_client.setSHost(InetAddress.getLocalHost().getHostName());
			m_client.setSAddress(InetAddress.getLocalHost().getHostAddress());
			m_client.setSName(sName);
			m_client.setOCallback(this);

			// Start client thread
            this.start();           
            return true;			
        } catch (RemoteException e) {
    		e.printStackTrace();
    		System.out.println("Chat server not available!");
    		System.exit(-1);
			return false;
        } catch (Exception e) {
        	System.err.println(e);
        	return false; 
        }
    }

	// Attach to server
	/* (non-Javadoc)
	 * @see chat.IClientIM#attach()
	 */
	public boolean attach(){
		try {
			// Attach to server
			m_client.setIIdentifier(m_remote.attach(m_client));
			
			// Not group leader
			m_group = null;

			if (m_client.getIIdentifier()==-1) return false;
			else return true;
		} catch (RemoteException e) {
    		e.printStackTrace();
    		System.out.println("Chat server not available!");
    		System.exit(-1);			
			return false;
		}
	}

	// Detach from server
	/* (non-Javadoc)
	 * @see chat.IClientIM#detach()
	 */
	public boolean detach(){
        try {
        	// Detach from server
        	m_remote.detach(m_client);

			// Not group leader
			m_group = null;

			return true;
        } catch (RemoteException e) {
    		e.printStackTrace();
    		System.out.println("Chat server not available!");
    		System.exit(-1);
        	return false;
        }
	}

	// Send chat message
	// @SpecData(allow={"ArgumentPassing"})
	/* (non-Javadoc)
	 * @see chat.IClientIM#send(java.lang.String)
	 */
	public void send(String sContents){
		Message oMessage = new Message();
		try { 
			// Send message to server
			oMessage.setSName(m_client.getSName());
			oMessage.setSContents(sContents);
			m_remote.send(oMessage);
		} 
		catch (RemoteException re) { 
    		re.printStackTrace();
    		System.out.println("Chat server not available!");
    		System.exit(-1);
		}
	}

	// Request server status
	/* (non-Javadoc)
	 * @see chat.IClientIM#dump()
	 */
	public Status dump() {
		Status status = null;
		try { 
			status = m_remote.dump();
		} 
		catch (RemoteException re) { 
    		re.printStackTrace();
    		System.out.println("Chat server not available!");
    		System.exit(-1);
		}
		return status;
	}

	// Abort client operation
	/* (non-Javadoc)
	 * @see chat.IClientIM#abort()
	 */
	public void abort(){
		m_abort = true;
	}

	// Return number of messages
	/* (non-Javadoc)
	 * @see chat.IClientIM#number()
	 */
	public int number(){
		return m_messages;
	}

	//
    // Private methods
    //
	
	// Transmit to clients
	// @SpecData(allow={"ArgumentPassing"})
	protected void retransmit(Message message){
		// Chat leader?
		if (m_group != null){
			Iterator<Entry<Integer, Client>> itClient = m_group.getOClients().entrySet().iterator();
			while (itClient.hasNext()) {
				Client client = itClient.next().getValue();
								
				// Transmit messages to group
				try { 
					// Transmit to each client
					client.getOCallback().transmit(message);
				} catch (RemoteException re) { 
					try { 
						// Detach missing clients
						m_remote.detach(client);
			    		re.printStackTrace();
			    		System.out.println("Chat server not available!");
			    		System.exit(-1);
		    		} catch (RemoteException re1) {
		        		re1.printStackTrace();
		        		System.out.println("Chat server not available!");
		        		System.exit(-1);
		    		}
				}
			}
		}
	}


}	
