package chat.impl;


import chat.IClient;
import chat.support.Encryption;

public class Scenario1ClientImpl extends StandardClientImpl {
	
	public Scenario1ClientImpl(IClient clientInterface) {
		super(clientInterface);
    }
	
	@Override
	public void send(String sContents){
		String encrypted = Encryption.encrypt(sContents);
		super.send(encrypted);
	}
}
