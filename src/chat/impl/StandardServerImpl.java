package chat.impl;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import chat.IServer;
import chat.IServerIM;
import chat.support.Client;
import chat.support.Group;
import chat.support.Message;
import chat.support.Status;
import chat.support.UsedNameException;

// Chat server implementation
public class StandardServerImpl extends Thread implements IServerIM {
	//
	// Member variables
	//
	
	protected ArrayList<String> nameList=new ArrayList<String>();
	
	// Default maximum size
	protected int m_iGroupMaximum = 0;
	
	// Number of clients attached
	protected int m_iNumberClients = 0;	

	// Message unique identifier, shows total number of messages processed
	protected int m_iMessageId = 0;
	
	// Client unique identifier
	protected int m_iClientId = 0;
	
	// User interface callbacks
	protected IServer m_ui;
	
	// Rmi registry object
	protected Registry m_registry;
	
	// Service name
	protected String m_service;			
	
	//
	// Not thread safe, server thread only accesses groups
	//
	protected TreeMap<Integer, Group> m_mapClients;	// Chat groups

	//
	// Thread safe, client is producer, server is consumer
	//
	// Server command
	protected class Command {
		public Command()
		{
			attach  = null;
			detach  = null;
			message = null;
		}
		Client attach;
		Client detach;
		Message message;
	}
	// Command queue
	protected ConcurrentLinkedQueue<Command> m_queueCommands;
	
	// Server constructor
	public StandardServerImpl(IServer serverInterface){
		// Store user interface callback
		m_ui = serverInterface;

		// Create chat groups
		m_mapClients = new TreeMap<Integer, Group>();
		
		// Create command queue
		m_queueCommands = new ConcurrentLinkedQueue<Command>();
    }
	
    //
    // Remote functions
    //

	// Add to attach queue
    public synchronized int attach(Client client) throws RemoteException {
    	int retValue=-1;
    	if(!nameList.contains(client.getSName())){
			nameList.add(client.getSName());
	    	// Assign unique identifier
			client.setIIdentifier(m_iClientId++);
	    	
	    	Command command = new Command();
	    	command.attach = client;
	    	m_queueCommands.add(command);		
			retValue=client.getIIdentifier();
			System.out.println(client.getSName() + " has joined");
		}
		else {
			throw new UsedNameException("username in use: "+client.getSName());
		}  	
    	notify(); 	
    	return retValue;
    }

    // Add to detach queue
    public synchronized void detach(Client client) throws RemoteException{
		Command command = new Command();
    	command.detach = client;
    	m_queueCommands.add(command);
    	nameList.remove(client.getSName());
    	System.out.println(client.getSName() + " has left");
    	notify();
    }

	// Add to message queue
    public synchronized void send(Message message) throws RemoteException {
    	message.setIIdentifier(m_iMessageId++);
    	Command command = new Command();
    	command.message = message;
    	m_queueCommands.add(command);
    	notify();
	}
    
    // Called by clients retrieve status
    public Status dump() throws RemoteException {
    	Status status = new Status();   		
    	status.setGroups(m_mapClients);
    	status.setIMessages(m_iMessageId);	
    	return status;
    }
    
    // Main server thread
	public synchronized void run(){
		// Send user interface message
		m_ui.message("Chat server is ready to receive chat clients");
		m_ui.message("Server maximum group size: "+ m_iGroupMaximum);

		// Check queues
		while (true){
			// Wait for commands
			try {
				wait();
			} catch (Exception e) {
			}

			// Process commands
			while (!m_queueCommands.isEmpty()) {
				Command command = m_queueCommands.remove();
				if (command.attach != null)	_attach(command.attach);
				else if (command.detach != null) _detach(command.detach);
				else if (command.message != null) {
					if (!_send(command.message)) _send(command.message);
				}
			}

			// Rebalance groups?
			_rebalance();
			
			// Update user interface
	    	m_ui.update(m_iNumberClients, m_mapClients.size(), m_iMessageId, m_iGroupMaximum);
		}
	}

    //
    // Public methods
    //

    // Export and run chat server
    public boolean initialize(int iGroupMaximum){ 
        // Create and install a security manager 
    	if (System.getSecurityManager() == null) System.setSecurityManager(new SecurityManager());
        
        // Store maximum size
        m_iGroupMaximum = iGroupMaximum;

        try {
        	// Define service name
        	m_service = "ChatServer";
        	
        	// Export server object
        	IServerIM stub = (IServerIM) UnicastRemoteObject.exportObject(this, 0);

        	// Locate rmi registry
    		m_registry = LocateRegistry.getRegistry();

    		// Bind stub to service 
            m_registry.rebind(m_service, stub);

            // Start server thread
            this.start();
            
            return true;
        } 
        catch (Exception e){ 
        	System.err.println("Blad serwera: " + e.toString());
        	e.printStackTrace();
        	return false;
        } 
    } 

    // Destroy chat server
    public void shutdown(){
    	// Unbind stub from service 
		try {
	        m_registry.unbind(m_service);
		} catch (Exception e) {
			// May not be able to unbind!
		}
    }

    // Called by user interface to get hierarchy
    public Status _dump(){
    	Status status = new Status(); 
    	
    	status.setGroups(m_mapClients);
    	status.setIMessages(m_iMessageId);
    	
    	return status;
    }

    //
    // Private functions
    //

	// Attach a client to server
    protected void _attach(Client client) {
		// Ignore if already attached
		if (_findClient(client) != null) {
			return;
		}		
		
		// Find group with with capacity
		Group group = _findRoom();
	    if (group != null) {
	    	// Insert into that group
	    	_insertClient(group, client);
	    }
	    else {
	    	// Create new group with client
	    	_createGroup(client);
	    }
    }

	// Detach a client from server
    protected void _detach(Client client) {
		// Is client chat leader?
    	if (m_mapClients.containsKey(client.getIIdentifier())) {
    		// Remove chat leader and replace
    		_removeLeader(client);
    	}
    	else {
    		// Find group and remove client
    		_removeClient(_findClient(client), client);
    	}
    }

	// Transmit message to groups
    protected boolean _send(Message message){
    	// Iterate groups
		Iterator<Entry<Integer, Group>> itGroup = m_mapClients.entrySet().iterator();
		while (itGroup.hasNext()) {
			Group group = itGroup.next().getValue();
			
    		// Transmit messages to leaders
    		try {
    			group.getOLeader().getOCallback().transmit(message);
    		} catch (RemoteException re) {
   				_detach(group.getOLeader());
    	    	return false;
    		}
    	}

    	// Send user interface message
    	// m_ui.message("Sending message " + message.sName + "> " + message.sContents);

    	return true;
	}

    // Create new group with client
    protected void _createGroup(Client client){
    	// Create group
    	Group group = new Group();

    	// Client is leader
    	group.setOLeader(client);
    	
    	// Empty client map
    	//group.setOClients(new TreeMap<Integer, Client>());
    	
    	// Add new group 
    	m_mapClients.put(client.getIIdentifier(), group);
    	// Increment number of clients
		m_iNumberClients++;

		// Update user interface
		m_ui.message("Create group, chat leader " + client.getSName() + ", id " + client.getIIdentifier());
    }

    // Find chat group with room
    protected Group _findRoom(){
    	// Iterate chat groups
    	Iterator<Entry<Integer, Group>> itGroup = m_mapClients.entrySet().iterator();
    	while (itGroup.hasNext()){
    		Group group = itGroup.next().getValue();
    		if (group.getOClients().size() < m_iGroupMaximum){
    			return group;
    		}
    	}
    	return null;
    }

	// Find chat group with client
    protected Group _findClient(Client client) {
    	// Iterate chat groups
    	Iterator<Entry<Integer, Group>> itGroup = m_mapClients.entrySet().iterator();
    	while (itGroup.hasNext()) {
    		Group group = itGroup.next().getValue();
    		if (group.getOClients().containsKey(client.getIIdentifier())){
    			return group;
    		}
    	}
    	return null;
    }

    // Insert client into chat group
    protected void _insertClient(Group group, Client client) {
    	// Check for null group
    	if (group == null) { 
    		return;
    	}

    	
    	// Insert client
    	group.getOClients().put(client.getIIdentifier(), client); 

    	// Increment number of clients
		m_iNumberClients++;

    	// Send user interface message
    	m_ui.message("Attach client " + client.getSName() + ", id " + client.getIIdentifier());

    	// Configure chat leader
		_configLeader(group);
	}

    // Remove client from chat group
    protected void _removeClient(Group group, Client client){
    	// Check for null group
    	if (group == null){
    		return;
    	}
    	
    	// Remove client
    	group.getOClients().remove(client.getIIdentifier());
    	
    	// Decrement number of clients
    	m_iNumberClients--;

    	// Send user interface message
    	m_ui.message("Detach client " + client.getSName()  + ", id " + client.getIIdentifier());
	
    	// Configure chat leader
		_configLeader(group);
    }

    // Remove client which is leader
    protected void _removeLeader(Client client){
    	Group group = m_mapClients.get(client.getIIdentifier());
    	if (group.getOClients().size() == 0) {
    		// Remove empty chat group
    		m_mapClients.remove(client.getIIdentifier());

    		// Send user interface message
    		m_ui.message("Destroy group with leader " + client.getSName() );
    	}
    	else {
    		_replaceLeader(group);
    	}
        // Decrement number of clients
    	m_iNumberClients--;
    }

    // Replace the current leader
    protected void _replaceLeader(Group group) {
		// Remove old group
		m_mapClients.remove(group.getOLeader().getIIdentifier());

		// Iterate chat clients
    	Iterator<Entry<Integer, Client>> itClient = group.getOClients().entrySet().iterator();

    	// Leader is initial client
		Client leader = itClient.next().getValue();
		
		// Remove leader from clients
		group.getOClients().remove(leader.getIIdentifier());
		
		// Send user interface message
		m_ui.message("Replacing leader " + group.getOLeader().getSName()  + " with " + leader.getSName() );

		// Replace group leader
		group.setOLeader(leader);

		// Replace group
		m_mapClients.put(leader.getIIdentifier(), group);
		
		// Configure new leader
		_configLeader(group);
	}

	// Configure group leader
    protected void _configLeader(Group group){
    	// Configure group leader
		try {
			group.getOLeader().getOCallback().configure(group);
		} catch (RemoteException re) {
			try {
				detach(group.getOLeader());
			} catch (RemoteException re1) {
				// Local call, no exceptions
			}
		}
    }

    // Rebalance chat groups
    protected void _rebalance(){
    	Group rebalance1 = null;
    	Group rebalance2 = null;
    
    	// Iterate chat groups
    	Iterator<Entry<Integer, Group>> itGroup = m_mapClients.entrySet().iterator();
    	while (itGroup.hasNext()) {
    		Group group = itGroup.next().getValue();
    		// Search for two groups with less than minimum
    		if (group.getOClients().size() <= (m_iGroupMaximum / 2)){
    			if (rebalance1 != null) rebalance2 = group;
    			if (rebalance1 == null) rebalance1 = group;
    		}
    	}

    	// Criteria for rebalance met?
    	if ((rebalance1 != null) && (rebalance2 != null)){
    		// Send user interface message
    		m_ui.message("Rebalancing groups, combining group " + rebalance2.getOLeader().getSName());
    		
    		// Combine both undersized groups
    		Client oLeader = rebalance2.getOLeader();
    		
    		// Iterate chat clients
    		Iterator<Entry<Integer, Client>> itClient = rebalance2.getOClients().entrySet().iterator();
    		while (itClient.hasNext()) {
        		Client client = itClient.next().getValue();
        		rebalance1.getOClients().put(client.getIIdentifier(), client);
    		}

    		// Empty rebalance group
    		rebalance2.getOClients().clear();

    	    // Reconfigure demoted group leader
    	    _configLeader(rebalance2);

    		// Add leader as normal client
    		rebalance1.getOClients().put(oLeader.getIIdentifier(), oLeader);
    		
    	    // Remove combined chat group
    		m_mapClients.remove(oLeader.getIIdentifier());
    	}
    }
}

