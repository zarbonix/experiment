package chat.impl;

import chat.IClient;

public class Scenario2ClientImpl extends Scenario1ClientImpl {

	public Scenario2ClientImpl(IClient clientInterface) {
		super(clientInterface);
	}
	
	public String getClientName() {
		return m_client.getSName();
	}
}	
