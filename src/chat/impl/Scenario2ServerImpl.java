package chat.impl;

import chat.IServer;
import chat.support.Client;
import chat.support.FileLogger;

// Chat server implementation
public class Scenario2ServerImpl extends StandardServerImpl {
	
	private final String LOGGER_FILE_NAME = "server.log";
	
	private final String JOINED_LOG = "has joined";
	
	private final String LEFT_LOG = "has left";
	
	protected FileLogger logger;
	
	public Scenario2ServerImpl(IServer serverInterface) {
		super(serverInterface);
		initLogger();
	}

	@Override
    protected void _attach(Client client) {
    	super._attach(client);
    	logger.log(generateJoinedLog(client));
    }

	@Override
    protected void _detach(Client client) {
		super._detach(client);
		logger.log(generateLeftLog(client));
    }
    
	protected void initLogger() {
		logger = new FileLogger(LOGGER_FILE_NAME);
	}
	
	protected String generateJoinedLog(Client client) {
		return client.getSName() + " " + JOINED_LOG;
	}
	
	protected String generateLeftLog(Client client) {
		return client.getSName() + " " + LEFT_LOG;
	}
}

