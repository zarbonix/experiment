package chat.impl;

import chat.IClient;
import chat.support.Scenario3Client;

public class Scenario3ClientImpl extends Scenario2ClientImpl {

	public Scenario3ClientImpl(IClient clientInterface) {
		super(clientInterface);
		m_client = new Scenario3Client();
	}
	
	public void setClientPassword(String password) {
		((Scenario3Client) m_client).setPassword(password);
	}
}	
