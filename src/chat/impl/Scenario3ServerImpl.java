package chat.impl;

import chat.IServer;
import chat.support.Client;
import chat.support.CredentialsLoader;
import chat.support.Scenario3Client;

// Chat server implementation
public class Scenario3ServerImpl extends Scenario2ServerImpl {

	public Scenario3ServerImpl(IServer serverInterface) {
		super(serverInterface);
	}

	@Override
    protected void _attach(Client client) {
    	if (isClientAuthorized(client)) {
    		super._attach(client);
    	} 
    }
	
	protected boolean isClientAuthorized(Client client) {
		Scenario3Client client3;
		try {
			client3 = (Scenario3Client) client;
		} catch (Exception e) {
			System.out.println("Klient ktory dokonal proby polaczenia nie jest klientem Scenario3Client");
			return false;
		}
		try {
			String password = CredentialsLoader.getCredentials().get(client3.getSName());
			if (password == null) {
				throw new Exception("Klient " + client3.getSName() + " nie jest w bazie autoryzowanych");
			}
			
			if (client3.getPassword().equals(password)) {
				return true;
			} else {
				throw new Exception("Bad password: " + client3.getSName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
}

