package chat.ui;

import java.awt.HeadlessException;
import java.rmi.RemoteException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import chat.impl.Scenario3ServerImpl;

public class Scenario3ServerUserInterface extends Scenario2ServerUserInterface {
	
	@Override
	public void initialize(){
		m_server = new Scenario3ServerImpl(this);
		
		// Export server
		try {
			if (!m_server.initialize(m_iGroupMaximum)){
				// Display dialog box
				JOptionPane.showMessageDialog(serverFrame,
											  "ChatServer unable to export remote service",
											  "ChatServer",
											  JOptionPane.WARNING_MESSAGE);
				System.exit(0);
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		// Parse arguments
		if (args.length >= 1) m_iGroupMaximum = Integer.parseInt(args[0]); 		
			
		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
			public void run() 
			{
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create and initialize user interface
				Scenario3ServerUserInterface userInterface = new Scenario3ServerUserInterface();

				// Initialize server
				userInterface.initialize();
			}
		});
	}
}
