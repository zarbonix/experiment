package chat.ui;

import javax.swing.JFrame;

import chat.impl.Scenario2ClientImpl;
import chat.support.FileLogger;

public class Scenario2ClientUserInterface extends Scenario1ClientUserInterface {
	
	protected FileLogger logger;
	
	@Override
	public void receive(String name, String contents){
		int linesBefore = outputText.getLineCount();
		super.receive(name, contents);
		int linesAfter = outputText.getLineCount();
		
		if (linesBefore >= linesAfter) {
			return;
		}
		logger.log(getLastAppendedMessages(linesBefore, linesAfter));
	}
	
	protected String getLastAppendedMessages(int linesBeforAppend, int linesAfterAppend) {
		try {
			int offsetStart = outputText.getLineStartOffset(linesBeforAppend - 1);
			int offsetStop = outputText.getLineEndOffset(linesAfterAppend - 1);
			int length = offsetStop - offsetStart - 1; // with removing the last char (next line character)
			return outputText.getText(offsetStart, length);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void initialize(){
		// Create client object
		m_client = new Scenario2ClientImpl(this);

		// Get chat name
		if ((m_sChatName == null) || (m_sChatName.length() == 0)){
			m_sChatName = getString("Chat name:");
		}
		
		// Get server name
		if ((m_sServerName == null) || (m_sServerName.length() == 0)){
			m_sServerName = getString("Server name:");
		}
		

		// Initialize client
		if (m_client.initialize(m_sServerName, m_sChatName)){
			// Attach to server
			if (m_client.attach()){
				// Display the window.
				clientFrame.pack();
				clientFrame.setVisible(true);
				return;
			}
		}

		// Warning dialog and exit
		if (m_sServerName == null){
			putString ("Invalid chat server selected");
		}	
		else{
			putString ("Cannot connect to chat server " + m_sServerName);
		}
		System.exit(0);
	}
	
	
	public static void main(String[] args) {
		// Parse arguments
		if (args.length >= 1) m_sChatName = args[0]; 
		if (args.length >= 2) m_sServerName = args[1]; 

		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create user interface
				Scenario2ClientUserInterface userInterface = new Scenario2ClientUserInterface();
				
				// Initialize client
				userInterface.initialize();
				userInterface.initLogger();
			}
		});
	}
	
	protected void initLogger() {
		logger = new FileLogger(getLoggerFileName());
	}
	
	protected String getLoggerFileName() {
		return getClientName() + ".log";
	}
	
	protected String getClientName() {
		return ((Scenario2ClientImpl) m_client).getClientName();
	}
}
