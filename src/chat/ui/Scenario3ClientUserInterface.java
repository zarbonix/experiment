package chat.ui;

import javax.swing.JFrame;

import chat.impl.Scenario3ClientImpl;

public class Scenario3ClientUserInterface extends Scenario2ClientUserInterface {
	
	public void initialize(String clientPassword){
		// Create client object
		Scenario3ClientImpl client = new Scenario3ClientImpl(this);
		m_client = client;
		client.setClientPassword(clientPassword);
		

		// Get chat name
		if ((m_sChatName == null) || (m_sChatName.length() == 0)){
			m_sChatName = getString("Chat name:");
		}
		
		// Get server name
		if ((m_sServerName == null) || (m_sServerName.length() == 0)){
			m_sServerName = getString("Server name:");
		}
		

		// Initialize client
		if (m_client.initialize(m_sServerName, m_sChatName)){
			// Attach to server
			if (m_client.attach()){
				// Display the window.
				clientFrame.pack();
				clientFrame.setVisible(true);
				return;
			}
		}

		// Warning dialog and exit
		if (m_sServerName == null){
			putString ("Invalid chat server selected");
		}	
		else{
			putString ("Cannot connect to chat server " + m_sServerName);
		}
		System.exit(0);
	}
	
	
	public static void main(String[] args) {
		
		// Parse arguments
		if (args.length >= 1) m_sChatName = args[0]; 
		if (args.length >= 2) m_sServerName = args[1]; 
		if (args.length < 3) 
			throw new RuntimeException("Password is not specify as the 3rd parameter");

		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create user interface
				Scenario3ClientUserInterface userInterface = new Scenario3ClientUserInterface();
				
				// Initialize client
				userInterface.initialize(args[2]);
				userInterface.initLogger();
			}
		});
	}
}
