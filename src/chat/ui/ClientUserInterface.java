package chat.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import chat.IClient;
import chat.IClientIM;
import chat.impl.StandardClientImpl;


public class ClientUserInterface extends Thread implements ActionListener, IClient {
	// Server name argument
	protected static String m_sServerName;

	// Chat name argument
	protected static String m_sChatName;

	// Chat client instance
	protected IClientIM m_client;			

	// User interface variables
	protected JFrame clientFrame;
	protected JPanel clientPanel;
	protected JLabel logoImage;
	protected JLabel labelOutput;
	protected JTextArea outputText;
	protected JLabel labelInput;
	protected JTextField inputText;
	protected JScrollPane outputScroll;
	protected JButton sendButton;
	protected JButton quitButton;

	// Create client user interface
	public ClientUserInterface(){
		// Create window
		clientFrame = new JFrame("Chat Client - " +m_sChatName);

		// Initialize window
		clientFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		clientFrame.setSize(new Dimension(600,500));

		// Create panel
		clientPanel = new JPanel();
		clientPanel.setLayout(new GridBagLayout());

		// Create widgets
		ImageIcon imageIcon = new ImageIcon("images/clientlogo.gif");
		logoImage = new JLabel(imageIcon);
		outputText = new JTextArea(10, 50);
		outputScroll = new JScrollPane(outputText);
		outputScroll.setPreferredSize(new Dimension(560, 300));
		inputText = new JTextField(50);
		sendButton = new JButton("Send");
		quitButton = new JButton("Quit");
		labelOutput = new JLabel("Chat Output:", SwingConstants.LEFT);
		labelInput = new JLabel("Chat Input:", SwingConstants.LEFT);

		// Add widgets
		GridBagConstraints constraint = new GridBagConstraints();
		constraint.fill = GridBagConstraints.HORIZONTAL;
		constraint.weightx = 0.5;
		constraint.gridx = 0;
		constraint.gridy = 0;
		constraint.gridwidth = 2;
		constraint.insets = new Insets(5,20,0,20);
		clientPanel.add(logoImage, constraint);
		constraint.gridx = 0;
		constraint.gridy = 1;
		constraint.ipady = 0; 
		clientPanel.add(labelOutput, constraint);
		constraint.gridx = 0;
		constraint.gridy = 2;
		clientPanel.add(outputScroll, constraint);
		constraint.gridx = 0;
		constraint.gridy = 3;
		clientPanel.add(labelInput, constraint);
		constraint.gridx = 0;
		constraint.gridy = 4;
		clientPanel.add(inputText, constraint);
		constraint.gridx = 0;
		constraint.gridy = 5;
		constraint.gridwidth = 1;
		constraint.insets = new Insets(5,60,5,60);
		clientPanel.add(sendButton, constraint);
		constraint.gridx = 1;
		constraint.gridy = 5;
		clientPanel.add(quitButton, constraint);

		// Add panels
		clientFrame.getContentPane().add(clientPanel);

		// Set font attributes
		Font font = new Font("Arial", Font.BOLD, 12);
		outputText.setForeground(new Color(64, 0, 255));
		outputText.setFont(font);
		
		// Cannot focus on output
		outputText.setFocusable(false);

		// Listen to events
		sendButton.addActionListener(this);
		quitButton.addActionListener(this);

		// Set the default button
		clientFrame.getRootPane().setDefaultButton(sendButton);
	}

	// Handler for button and keyboard input
	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand().equals("Quit")){
			// Shutdown client
			shutdown();
			
			System.exit(0);
		}
		else if (event.getActionCommand().equals("Send")){
			// Send message and clear input text
			send(inputText.getText());
			inputText.setText("");
		}
	}

	//
	// Public Methods
	// 

	// Initialize chat client
	public void initialize(){
		// Create client object
		m_client = new StandardClientImpl(this);


		// Get chat name
		if ((m_sChatName == null) || (m_sChatName.length() == 0)){
			m_sChatName = getString("Chat name:");
		}
		
		// Get server name
		if ((m_sServerName == null) || (m_sServerName.length() == 0)){
			m_sServerName = getString("Server name:");
		}
		

		// Initialize client
		if (m_client.initialize(m_sServerName, m_sChatName)){
			// Attach to server
			if (m_client.attach()){
				// Display the window.
				clientFrame.pack();
				clientFrame.setVisible(true);
				return;
			}
		}

		// Warning dialog and exit
		if (m_sServerName == null){
			putString ("Invalid chat server selected");
		}	
		else{
			putString ("Cannot connect to chat server " + m_sServerName);
		}
		System.exit(0);
	}

	// Ask chat client to detach and exit
	public void shutdown(){
		m_client.detach();
	}

	// Ask chat client to send a chat message
	public void send(String contents){
		m_client.send(contents);
	}

	// Client is sending an error
	public void error(String contents){
		// Notify user about error
		putString(contents);
	}

	// Client is sending a warning
	public void warning(String warning){
		// Add warning to output area
		outputText.append(warning + System.lineSeparator());
	}

	// Client is sending a message
	public void receive(String name, String contents){
		// Display message
		outputText.append(name + "> " + contents + System.lineSeparator());

		// Scroll to bottom
		outputText.setCaretPosition(outputText.getDocument().getLength());
	}
	
	//
	// Private Methods
	// 
	
	// Utility to get user input
	protected String getString(String prompt){
		// Display dialog box
		return (String)JOptionPane.showInputDialog(clientFrame,
												   prompt,
												   "Chat client",
												   JOptionPane.QUESTION_MESSAGE,
												   null,
												   null,
												   "");
	}

	// Utility to put up a warning message
	protected void putString(String contents){
		// Display dialog box
		JOptionPane.showMessageDialog(clientFrame,
									  contents,
									  "ChatClient",
									  JOptionPane.WARNING_MESSAGE);
	}

	//
	// Main entry point for client
	//
	public static void main(String[] args) {
		// Parse arguments
		if (args.length >= 1) m_sChatName = args[0]; 
		if (args.length >= 2) m_sServerName = args[1]; 

		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create user interface
				ClientUserInterface userInterface = new ClientUserInterface();
				
				// Initialize client
				userInterface.initialize();
			}
		});
	}
}
