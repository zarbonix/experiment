package chat.ui;

import javax.swing.JFrame;

import chat.impl.Scenario1ClientImpl;
import chat.support.Encryption;

public class Scenario1ClientUserInterface extends ClientUserInterface {
	
	@Override
	public void receive(String name, String contents){
		String decrypted = Encryption.decrypt(contents);
		super.receive(name, decrypted);
	}
	
	// Initialize chat client
	@Override
	public void initialize(){
		// Create client object
		m_client = new Scenario1ClientImpl(this);

		// Get chat name
		if ((m_sChatName == null) || (m_sChatName.length() == 0)){
			m_sChatName = getString("Chat name:");
		}
		
		// Get server name
		if ((m_sServerName == null) || (m_sServerName.length() == 0)){
			m_sServerName = getString("Server name:");
		}
		

		// Initialize client
		if (m_client.initialize(m_sServerName, m_sChatName)){
			// Attach to server
			if (m_client.attach()){
				// Display the window.
				clientFrame.pack();
				clientFrame.setVisible(true);
				return;
			}
		}

		// Warning dialog and exit
		if (m_sServerName == null){
			putString ("Invalid chat server selected");
		}	
		else{
			putString ("Cannot connect to chat server " + m_sServerName);
		}
		System.exit(0);
	}

	public static void main(String[] args) {
		// Parse arguments
		if (args.length >= 1) m_sChatName = args[0]; 
		if (args.length >= 2) m_sServerName = args[1]; 

		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create user interface
				Scenario1ClientUserInterface userInterface = new Scenario1ClientUserInterface();
				
				// Initialize client
				userInterface.initialize();
			}
		});
	}
}
